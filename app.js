const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');

const app = express();
const port = 8000;

var corsOptions = {
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  optionsSuccessStatus: 200
};

app.use(cors(corsOptions));
app.use(bodyParser.json());

app.use(fileUpload());

/**
 * Try GET rest api
 */
app.route('/test').get((req, res) => {
  res.send({ value: 'hello clover' });
  console.log(req.query.id);
});

/**
 * Try POST API
 */
app.route('/trypost').post((req, res) => {
  console.log(req.body);
  res.send(req.body);
});

/**
 * Try upload file
 */
app.route('/upload-file').post(async (req, res) => {
  console.log(req.files.thefile);
  console.log(req.body.data);

  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were upload');
  }

  let sampleFile = req.files.thefile;

  sampleFile.mv('./tmp/' + sampleFile.name, err => {
    if (err) {
      return res.status(500).send(err);
    }

    res.send({ result: 'File uploaded!' });
  });
});

app.listen(port, () => console.log(`example app listening on port ${port}`));
